﻿using UnityEngine;
using Assets.Scripts.Utils;
using System;

public class RobotMover : MonoBehaviour
{
    private void Awake()
    {
        cameraOffset = new Vector3(0.0f, 0.5f, 2.3f);
    }

    private void Start()
    {
        Camera.main.transform.position = transform.position + cameraOffset;
        Camera.main.transform.LookAt(_spawner.Base.transform);

        _spawner.RightShoulderNode.transform.rotation = Quaternion.Euler(new Vector3(-10.0f, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z));
        _spawner.LeftShoulderNode.transform.rotation = Quaternion.Euler(new Vector3(10.0f, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z));

        _spawner.RightButtcheekNode.transform.rotation = Quaternion.Euler(new Vector3(10.0f, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z));
        _spawner.LeftButtcheekNode.transform.rotation = Quaternion.Euler(new Vector3(-10.0f, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z));

        _spawner.RightKneeNode.transform.rotation = Quaternion.Euler(new Vector3(-10.0f, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z));
        _spawner.LeftKneeNode.transform.rotation = Quaternion.Euler(new Vector3(10.0f, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z));
    }

    private void BobHead()
    {
        float headAngleX = _spawner.TorsoNode.transform.rotation.eulerAngles.x;

        float time = Mathf.PingPong(Time.time * headBobSpeed * globalSpeed, 1.0f);
        _spawner.NeckNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(headBobAngleMin + headAngleX, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), Quaternion.Euler(headBobAngleMax + headAngleX, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), time);
        //float time = Time.time;
        //float timePingPong = Mathf.PingPong(time, 1.0f);

        //float headAngle = Mathf.LerpAngle(angleNegative, anglePositive, timePingPong);
        //_spawner.NeckNode.transform.rotation = Quaternion.Euler(rotationAxis * headAngle * headBobSpeed * globalSpeed + _spawner.Base.transform.rotation.eulerAngles);
    }

    //private void CircleAroundSpawn()
    //{
    //    float angle = timer;
    //    Vector3 basePos = _spawner.transform.position;
    //    _spawner.Base.transform.position = new Vector3(basePos.x + Mathf.Sin(angle) * circleRadius, _spawner.Base.transform.position.y, basePos.z + Mathf.Cos(angle) * circleRadius);

    //    _spawner.Base.transform.Rotate(new Vector3(0.0f, circlingSpeed * Time.deltaTime, 0.0f), Space.World);
    //}

    private void XRotateLeftUpperLimb()
    {
        float t = Mathf.PingPong(Time.time * armRotateSpeed * globalSpeed, 1.0f);
        _spawner.LeftShoulderNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(armAngleMin, _spawner.Torso.transform.rotation.eulerAngles.y, _spawner.Torso.transform.rotation.eulerAngles.z), Quaternion.Euler(armAngleMax, _spawner.Torso.transform.rotation.eulerAngles.y, _spawner.Torso.transform.rotation.eulerAngles.z), t);
    }

    private void XRotateLeftForearm()
    {
        float armAngleX = _spawner.LeftShoulderNode.transform.rotation.eulerAngles.x;

        float time = Mathf.PingPong(Time.time * armRotateSpeed * globalSpeed, 2.0f);
        _spawner.LeftElbowNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(forearmAngleMin + armAngleX, _spawner.Torso.transform.rotation.eulerAngles.y, _spawner.Torso.transform.rotation.eulerAngles.z), Quaternion.Euler(forearmAngleMax + armAngleX, _spawner.Torso.transform.rotation.eulerAngles.y, _spawner.Torso.transform.rotation.eulerAngles.z), time);
    }

    private void XRotateRightUpperLimb()
    {
        float t = Mathf.PingPong(Time.time * armRotateSpeed * globalSpeed, 1.0f);
        _spawner.RightShoulderNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(armAngleMax, _spawner.Torso.transform.rotation.eulerAngles.y, _spawner.Torso.transform.rotation.eulerAngles.z), Quaternion.Euler(armAngleMin, _spawner.Torso.transform.rotation.eulerAngles.y, _spawner.Torso.transform.rotation.eulerAngles.z), t);
    }

    private void XRotateRightForearm()
    {
        float armAngleX = _spawner.RightShoulderNode.transform.rotation.eulerAngles.x;

        float time = Mathf.PingPong(Time.time * armRotateSpeed * globalSpeed, 2.0f);
        _spawner.RightElbowNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(forearmAngleMin + armAngleX, _spawner.Torso.transform.rotation.eulerAngles.y, _spawner.Torso.transform.rotation.eulerAngles.z), Quaternion.Euler(forearmAngleMax + armAngleX, _spawner.Torso.transform.rotation.eulerAngles.y, _spawner.Torso.transform.rotation.eulerAngles.z), time);
    }

    private void XRotateLeftLowerLimb()
    {
        float t = Mathf.PingPong(Time.time * legRotateSpeed * globalSpeed, 1.0f);
        _spawner.LeftButtcheekNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(legAngleMax, _spawner.Pelvis.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), Quaternion.Euler(legAngleMin, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), t);
    }

    private void XRotateLeftCrus()
    {
        float legAngleX = _spawner.LeftButtcheekNode.transform.rotation.eulerAngles.x;

        float time = Mathf.PingPong(Time.time * legRotateSpeed * globalSpeed, 1.0f);
        _spawner.LeftKneeNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(crusAngleMin + legAngleX, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), Quaternion.Euler(crusAngleMax + legAngleX, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), time);
    }

    private void XRotateRightLowerLimb()
    {
        float t = Mathf.PingPong(Time.time * legRotateSpeed * globalSpeed, 1.0f);
        _spawner.RightButtcheekNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(legAngleMin, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), Quaternion.Euler(legAngleMax, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), t);
    }

    private void XRotateRightCrus()
    {
        float legAngleX = _spawner.RightButtcheekNode.transform.rotation.eulerAngles.x;

        float time = Mathf.PingPong(Time.time * legRotateSpeed * globalSpeed, 1.0f);
        _spawner.RightKneeNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(crusAngleMax + legAngleX, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), Quaternion.Euler(crusAngleMin + legAngleX, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), time);
    }

    private void XRotateRightAnkle()
    {
        float ankleAngleX = _spawner.RightKneeNode.transform.rotation.eulerAngles.x;

        float time = Mathf.PingPong(Time.time * legRotateSpeed * globalSpeed, 1.0f);
        _spawner.RightAnkleNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(ankleAngleMax + ankleAngleX, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), Quaternion.Euler(ankleAngleMin + ankleAngleX, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), time);
    }

    private void XRotateLeftAnkle()
    {
        float ankleAngleX = _spawner.LeftKneeNode.transform.rotation.eulerAngles.x;

        float time = Mathf.PingPong(Time.time * legRotateSpeed * globalSpeed, 1.0f);
        _spawner.LeftAnkleNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(ankleAngleMin + ankleAngleX, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), Quaternion.Euler(ankleAngleMax + ankleAngleX, _spawner.Base.transform.rotation.eulerAngles.y, _spawner.Base.transform.rotation.eulerAngles.z), time);
    }

    private void XFloatPelvis()
    {
        float time = Mathf.PingPong(Time.time * globalSpeed, 1.0f);
        _spawner.Pelvis.transform.position += new Vector3(0.0f, Mathf.Lerp(-0.0005f * globalSpeed, 0.0005f * globalSpeed, time), 0.0f);
    }

    private void YRotatePelvis()
    {
        float baseAngleY = _spawner.Base.transform.rotation.eulerAngles.y;
        float time = Mathf.PingPong(Time.time * globalSpeed, 1.0f);
        _spawner.PelvisNode.transform.rotation = Quaternion.Slerp(Quaternion.Euler(_spawner.Base.transform.rotation.eulerAngles.x, torsoAngleMin + baseAngleY, _spawner.Base.transform.rotation.eulerAngles.z), Quaternion.Euler(_spawner.Base.transform.rotation.eulerAngles.x, torsoAngleMax + baseAngleY, _spawner.Base.transform.rotation.eulerAngles.z), time);
    }

    private void Update()
    {
        if (armAngleMax >= Constants.ARM_ANGLE_MAX)
            armAngleMax = Constants.ARM_ANGLE_MAX;

        _timer += Time.deltaTime;
        if (!_spawner)
            return;

        float translation = -Input.GetAxis("Vertical") * globalSpeed * Time.deltaTime;
        float rotation = Input.GetAxis("Horizontal") * 100.0f * Time.deltaTime;
        if (translation != 0.0f)
        {
            if (rotation != 0.0f)
                _spawner.Base.transform.Rotate(0, rotation, 0);

            _spawner.Base.transform.Translate(0, 0, translation);
            Camera.main.transform.LookAt(_spawner.Base.transform);
            Camera.main.transform.position = _spawner.Base.transform.position + cameraOffset;


            XRotateLeftUpperLimb();
            XRotateLeftForearm();

            XRotateRightUpperLimb();
            XRotateRightForearm();


            XRotateLeftLowerLimb();
            XRotateLeftCrus();
            XRotateLeftAnkle();

            XRotateRightLowerLimb();
            XRotateRightCrus();
            XRotateRightAnkle();
        }
        else if (rotation != 0.0f)
                _spawner.Base.transform.Rotate(0, rotation, 0);

        BobHead();
        YRotatePelvis();
        XFloatPelvis();
    }

    public RobotSpawner Spawner
    {
        get
        {
            return _spawner;
        }
        set
        {
            _spawner = value;
        }
    }

    private RobotSpawner _spawner;

    private float _timer = 0.0f;

    //public float circleRadius = 1.0f;
    //public float circlingSpeed = 60.0f;

    public float globalSpeed = 1.15f;
    public float armRotateSpeed = 1.0f;
    public float legRotateSpeed = 1.0f;
    public float headBobSpeed = 1.0f;

    public float armAngleMin = -20.0f;
    public float armAngleMax = 40.0f;
    public float forearmAngleMin = 10.0f;
    public float forearmAngleMax = 20.0f;

    public float legAngleMin = -50.0f;
    public float legAngleMax = 50.0f;
    public float crusAngleMin = -30.0f;
    public float crusAngleMax = 20.0f;
    public float ankleAngleMin = 10.0f;
    public float ankleAngleMax = -15.0f;

    public float torsoAngleMin = -5.0f;
    public float torsoAngleMax = 5.0f;

    public float headBobAngleMin = -15.0f;
    public float headBobAngleMax = 0.0f;

    public Vector3 cameraOffset;
}
