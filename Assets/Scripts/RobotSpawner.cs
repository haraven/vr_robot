﻿using UnityEngine;

public class RobotSpawner : MonoBehaviour
{
    private void InitTorso()
    {
        _pelvis_node = new GameObject("Pelvis");
        _pelvis_node.transform.parent = _base.transform;
        _pelvis_node.transform.position = _base.transform.position;
        _pelvis = (GameObject)Instantiate
        (
            robotPelvisPrefab,
            _pelvis_node.transform.position + Vector3.down / 10 + Vector3.back / 20.0f,
            _pelvis_node.transform.rotation,
            _pelvis_node.transform
        );

        _torso_node = new GameObject("Torso");
        _torso_node.transform.parent = _pelvis_node.transform;
        _torso_node.transform.position = _pelvis_node.transform.position + Vector3.back / 20.0f + Vector3.up / 1.6f * _pelvis.transform.localScale.y;
        _torso = (GameObject)Instantiate
        (
            robotTorsoPrefab,
            _torso_node.transform.position,
            _torso_node.transform.rotation,
            _torso_node.transform
        );
    }

    private void InitLeftUpperLimb()
    {
        _left_shoulder_node = new GameObject("Left shoulder");
        _left_shoulder_node.transform.parent = _torso_node.transform;
        _left_shoulder_node.transform.position = Torso.transform.position + Vector3.left / 1.7f * Torso.transform.localScale.x + Vector3.up / 3.0f * Torso.transform.localScale.y;
        _left_shoulder_node.transform.rotation = _torso_node.transform.rotation;

        _left_shoulder = (GameObject)Instantiate
        (
            robotShoulderPrefab,
            _left_shoulder_node.transform.position,
            _left_shoulder_node.transform.rotation,
            _left_shoulder_node.transform
        );

        _left_arm = (GameObject)Instantiate
        (
            robotArmPrefab,
            _left_shoulder.transform.position + Vector3.down / 1.5f * _left_shoulder.transform.localScale.y + Vector3.left / 20,
            _left_shoulder.transform.rotation,
            _left_shoulder_node.transform
        );
        _left_arm.transform.Rotate(0.0f, 0.0f, -19.0f);

        _left_elbow_node = new GameObject("Left elbow");
        _left_elbow_node.transform.parent = _left_shoulder_node.transform;
        _left_elbow_node.transform.position = _left_arm.transform.position + Vector3.down / 1.8f * _left_arm.transform.localScale.y + Vector3.left / 18;

        _left_elbow = (GameObject)Instantiate
        (
            robotElbowPrefab,
            _left_elbow_node.transform.position,
            _left_elbow_node.transform.rotation,
            _left_elbow_node.transform
        );

        _left_forearm = (GameObject)Instantiate
        (
            robotForearmPrefab,
            _left_elbow_node.transform.position + Vector3.down * 2.3f * _left_elbow.transform.localScale.y,
            _left_elbow_node.transform.rotation,
            _left_elbow_node.transform
        );

        _left_wrist = (GameObject)Instantiate
        (
            robotWristPrefab,
            _left_forearm.transform.position + Vector3.down / 1.7f * _left_forearm.transform.localScale.y,
            _left_forearm.transform.rotation,
            _left_elbow_node.transform
        );
    }

    private void InitRightUpperLimb()
    {
        _right_shoulder_node = new GameObject("Right shoulder");
        _right_shoulder_node.transform.parent = _torso_node.transform;
        _right_shoulder_node.transform.position = _torso.transform.position + Vector3.right / 1.7f * _torso.transform.localScale.x + Vector3.up / 3.0f * _torso.transform.localScale.y;
        _right_shoulder_node.transform.rotation = _torso_node.transform.rotation;

        _right_shoulder = (GameObject)Instantiate
        (
            robotShoulderPrefab,
            _right_shoulder_node.transform.position,
            Torso.transform.rotation,
            _right_shoulder_node.transform
        );

        _right_arm = (GameObject)Instantiate
        (
            robotArmPrefab,
            _right_shoulder.transform.position + Vector3.down / 1.5f * _right_shoulder.transform.localScale.y + Vector3.right / 20,
            _right_shoulder.transform.rotation,
            _right_shoulder_node.transform
        );
        _right_arm.transform.Rotate(0.0f, 0.0f, 19.0f);

        _right_elbow_node = new GameObject("Right elbow");
        _right_elbow_node.transform.parent = _right_shoulder_node.transform;
        _right_elbow_node.transform.position = _right_arm.transform.position + Vector3.down / 1.8f * _right_arm.transform.localScale.y + Vector3.right / 18;

        _right_elbow = (GameObject)Instantiate
        (
            robotElbowPrefab,
            _right_elbow_node.transform.position,
            _right_elbow_node.transform.rotation,
            _right_elbow_node.transform
        );

        _right_forearm = (GameObject)Instantiate
        (
            robotForearmPrefab,
            _right_elbow_node.transform.position + Vector3.down * 2.3f * _right_elbow.transform.localScale.y,
            _right_elbow_node.transform.rotation,
            _right_elbow_node.transform
        );

        _right_wrist = (GameObject)Instantiate
        (
            robotWristPrefab,
            _right_forearm.transform.position + Vector3.down / 1.7f * _right_forearm.transform.localScale.y,
            _right_forearm.transform.rotation,
            _right_elbow_node.transform
        );
    }

    private void InitUpperLimbs()
    {
        if (!Torso)
        {
            Debug.LogError("Tried to initialize upper limbs before torso. Aborting");
            return;
        }

        InitLeftUpperLimb();
        InitRightUpperLimb();
    }

    private void InitLowerLimbs()
    {
        if (!Torso)
        {
            Debug.LogError("Tried to initialize lower limbs before torso. Aborting");
            return;
        }

        InitLeftLowerLimb();
        InitRightLowerLimb();
    }

    private void InitRightLowerLimb()
    {
        _right_buttcheek_node = new GameObject("Right buttcheek");
        _right_buttcheek_node.transform.position = Pelvis.transform.position + Vector3.down * Pelvis.transform.localScale.y / 1.7f + Vector3.right * Pelvis.transform.localScale.x / 3;
        _right_buttcheek_node.transform.parent = _pelvis_node.transform.parent;
        _right_buttcheek_node.transform.rotation = _pelvis_node.transform.rotation;

        _right_buttcheek = (GameObject)Instantiate
        (
            robotButtcheekPrefab,
            Pelvis.transform.position + Vector3.forward / 35 + Vector3.down / 5.0f + Vector3.right / 10.0f,
            Pelvis.transform.rotation,
            _right_buttcheek_node.transform
        );

        _right_thigh = (GameObject)Instantiate
        (
            robotThighPrefab,
            _right_buttcheek.transform.position + Vector3.back / 30 + Vector3.down * Pelvis.transform.localScale.y / 1.5f,
            _right_buttcheek.transform.rotation,
            _right_buttcheek_node.transform
        );
        _right_thigh.transform.Rotate(10.0f, 0.0f, 0.0f);

        _right_knee_node = new GameObject("Right knee");
        _right_knee_node.transform.parent = _right_buttcheek_node.transform;
        _right_knee_node.transform.position = _right_thigh.transform.position + Vector3.back / 25 + Vector3.down / 1.7f * _right_thigh.transform.localScale.y;

        _right_knee = (GameObject)Instantiate
        (
            robotKneePrefab,
            _right_knee_node.transform.position,
            _right_knee_node.transform.rotation,
            _right_knee_node.transform
        );

        _right_crus = (GameObject)Instantiate
        (
            robotCrusPrefab,
            _right_knee.transform.position + Vector3.back * -0.04f + Vector3.down * 1.75f * _right_knee.transform.localScale.y,
            _right_knee.transform.rotation,
            _right_knee_node.transform
        );
        _right_crus.transform.Rotate(-15.0f, 0.0f, 0.0f);

        _right_ankle_node = new GameObject("Right ankle");
        _right_ankle_node.transform.parent = _right_knee_node.transform;
        _right_ankle_node.transform.position = _right_crus.transform.position + Vector3.down / 6 + Vector3.back * -0.05f;

        _right_ankle = (GameObject)Instantiate
        (
            robotAnklePrefab,
            _right_ankle_node.transform.position,
            _right_ankle_node.transform.rotation,
            _right_ankle_node.transform.transform
        );

        _right_foot = (GameObject)Instantiate
        (
            robotFootPrefab,
            _right_ankle_node.transform.position + Vector3.down / 1.2f * _right_ankle.transform.localScale.y + Vector3.back * 0.05f,
            Quaternion.Euler(Vector3.zero),
            _right_ankle_node.transform
        );
    }

    private void InitLeftLowerLimb()
    {
        _left_buttcheek_node = new GameObject("Left buttcheek");
        _left_buttcheek_node.transform.position = Pelvis.transform.position + Vector3.down * Pelvis.transform.localScale.y / 1.7f + Vector3.left * Pelvis.transform.localScale.x / 3;
        _left_buttcheek_node.transform.parent = _pelvis_node.transform.parent;
        _left_buttcheek_node.transform.rotation = _pelvis_node.transform.rotation;

        _left_buttcheek = (GameObject)Instantiate
        (
            robotButtcheekPrefab,
            Pelvis.transform.position + Vector3.forward / 35 + Vector3.down / 5.0f + Vector3.left / 10.0f,
            Pelvis.transform.rotation,
            _left_buttcheek_node.transform
        );

        _left_thigh = (GameObject)Instantiate
        (
            robotThighPrefab,
            _left_buttcheek.transform.position + Vector3.back / 30 + Vector3.down * Pelvis.transform.localScale.y / 1.5f,
            _left_buttcheek.transform.rotation,
            _left_buttcheek_node.transform
        );
        _left_thigh.transform.Rotate(10.0f, 0.0f, 0.0f);

        _left_knee_node = new GameObject("Left knee");
        _left_knee_node.transform.parent = _left_buttcheek_node.transform;
        _left_knee_node.transform.position = _left_thigh.transform.position + Vector3.back / 25 + Vector3.down / 1.7f * _left_thigh.transform.localScale.y;

        _left_knee = (GameObject)Instantiate
        (
            robotKneePrefab,
            _left_knee_node.transform.position,
            _left_knee_node.transform.rotation,
            _left_knee_node.transform
        );

        _left_crus = (GameObject)Instantiate
        (
            robotCrusPrefab,
            _left_knee.transform.position + Vector3.back * -0.04f + Vector3.down * 1.75f * _left_knee.transform.localScale.y,
            _left_knee.transform.rotation,
            _left_knee_node.transform
        );
        _left_crus.transform.Rotate(-15.0f, 0.0f, 0.0f);

        _left_ankle_node = new GameObject("Left ankle");
        _left_ankle_node.transform.parent = _left_knee_node.transform;
        _left_ankle_node.transform.position = _left_crus.transform.position + Vector3.down / 6 + Vector3.back * -0.05f;

        _left_ankle = (GameObject)Instantiate
        (
            robotAnklePrefab,
            _left_ankle_node.transform.position,
            _left_ankle_node.transform.rotation,
            _left_ankle_node.transform.transform
        );

        _left_foot = (GameObject)Instantiate
        (
            robotFootPrefab,
            _left_ankle_node.transform.position + Vector3.down / 1.2f * _left_ankle.transform.localScale.y + Vector3.back * 0.05f,
            Quaternion.Euler(Vector3.zero),
            _left_ankle_node.transform
        );
    }

    private void InitHead()
    {
        if (!Torso)
        {
            Debug.LogError("Tried to initialize head before torso. Aborting");
            return;
        }

        _neck_node = new GameObject("Robot head");
        _neck_node.transform.position = _torso.transform.position + Vector3.up / 1.7f * Torso.transform.localScale.y;
        _neck_node.transform.rotation = _torso_node.transform.rotation;
        _neck_node.transform.parent = _torso_node.transform;

        _neck = (GameObject)Instantiate
        (
            robotNeckPrefab,
            _neck_node.transform.position,
            _neck_node.transform.rotation,
            _neck_node.transform
        );

        _head = (GameObject)Instantiate
        (
            robotHeadPrefab,
            Neck.transform.position + Vector3.up / 10,
            Neck.transform.rotation,
            _neck_node.transform
        );
    }

    void Start()
    {
        _base = new GameObject("Robot");
        _base.transform.position = Vector3.up * 1.159f;
        InitTorso();
        InitHead();
        InitUpperLimbs();
        InitLowerLimbs();

        var mover = _base.AddComponent<RobotMover>();
        mover.Spawner = this;
    }

    public GameObject NeckNode
    {
        get
        {
            return _neck_node;
        }
    }

    public GameObject LeftShoulderNode
    {
        get
        {
            return _left_shoulder_node;
        }
    }

    public GameObject RightShoulderNode
    {
        get
        {
            return _right_shoulder_node;
        }
    }

    public GameObject LeftElbowNode
    {
        get
        {
            return _left_elbow_node;
        }
    }

    public GameObject RightElbowNode
    {
        get
        {
            return _right_elbow_node;
        }
    }

    public GameObject LeftButtcheekNode
    {
        get
        {
            return _left_buttcheek_node;
        }
    }

    public GameObject RightButtcheekNode
    {
        get
        {
            return _right_buttcheek_node;
        }
    }

    public GameObject LeftKneeNode
    {
        get
        {
            return _left_knee_node;
        }
    }

    public GameObject RightKneeNode
    {
        get
        {
            return _right_knee_node;
        }
    }

    public GameObject LeftAnkleNode
    {
        get
        {
            return _left_ankle_node;
        }
    }

    public GameObject RightAnkleNode
    {
        get
        {
            return _right_ankle_node;
        }
    }

    public GameObject TorsoNode
    {
        get
        {
            return _torso_node;
        }
    }

    public GameObject PelvisNode
    {
        get
        {
            return _pelvis_node;
        }
    }

    public GameObject Base
    {
        get
        {
            return _base;
        }
    }

    public GameObject Head
    {
        get
        {
            return _head;
        }
    }

    public GameObject Neck
    {
        get
        {
            return _neck;
        }
    }

    public GameObject Torso
    {
        get
        {
            return _torso;
        }
    }

    public GameObject Pelvis
    {
        get
        {
            return _pelvis;
        }
    }

    public GameObject Left_shoulder
    {
        get
        {
            return _left_shoulder;
        }
    }

    public GameObject Left_arm
    {
        get
        {
            return _left_arm;
        }
    }

    public GameObject Left_elbow
    {
        get
        {
            return _left_elbow;
        }
    }

    public GameObject Left_forearm
    {
        get
        {
            return _left_forearm;
        }
    }

    public GameObject RightShoulder
    {
        get
        {
            return _right_shoulder;
        }
    }

    public GameObject RightArm
    {
        get
        {
            return _right_arm;
        }
    }

    public GameObject RightElbow
    {
        get
        {
            return _right_elbow;
        }
    }

    public GameObject RightForearm
    {
        get
        {
            return _right_forearm;
        }
    }

    public GameObject LeftButtcheek
    {
        get
        {
            return _left_buttcheek;
        }
    }

    public GameObject LeftThigh
    {
        get
        {
            return _left_thigh;
        }
    }

    public GameObject LeftKnee
    {
        get
        {
            return _left_knee;
        }
    }

    public GameObject LeftCrus
    {
        get
        {
            return _left_crus;
        }
    }

    public GameObject Left_ankle
    {
        get
        {
            return _left_ankle;
        }
    }

    public GameObject Left_foot
    {
        get
        {
            return _left_foot;
        }
    }

    public GameObject RightButtcheek
    {
        get
        {
            return _right_buttcheek;
        }
    }

    public GameObject RightThigh
    {
        get
        {
            return _right_thigh;
        }
    }

    public GameObject RightKnee
    {
        get
        {
            return RightKnee;
        }
    }

    public GameObject RightCrus
    {
        get
        {
            return _right_crus;
        }
    }

    public GameObject RightAnkle
    {
        get
        {
            return _right_ankle;
        }
    }

    public GameObject RightFoot
    {
        get
        {
            return _right_foot;
        }
    }

    public GameObject robotHeadPrefab;
    public GameObject robotNeckPrefab;
    public GameObject robotTorsoPrefab;
    public GameObject robotPelvisPrefab;
    public GameObject robotShoulderPrefab;
    public GameObject robotArmPrefab;
    public GameObject robotElbowPrefab;
    public GameObject robotForearmPrefab;
    public GameObject robotWristPrefab;
    public GameObject robotButtcheekPrefab;
    public GameObject robotThighPrefab;
    public GameObject robotKneePrefab;
    public GameObject robotCrusPrefab;
    public GameObject robotAnklePrefab;
    public GameObject robotFootPrefab;

    private GameObject _base;

    private GameObject _head;
    private GameObject _neck;

    private GameObject _torso;
    private GameObject _pelvis;

    private GameObject _left_shoulder;
    private GameObject _left_arm;
    private GameObject _left_elbow;
    private GameObject _left_forearm;
    private GameObject _left_wrist;

    private GameObject _right_shoulder;
    private GameObject _right_arm;
    private GameObject _right_elbow;
    private GameObject _right_forearm;
    private GameObject _right_wrist;

    private GameObject _left_buttcheek;
    private GameObject _left_thigh;
    private GameObject _left_knee;
    private GameObject _left_crus;
    private GameObject _left_ankle;
    private GameObject _left_foot;

    private GameObject _right_buttcheek;
    private GameObject _right_thigh;
    private GameObject _right_knee;
    private GameObject _right_crus;
    private GameObject _right_ankle;
    private GameObject _right_foot;

    // invisible nodes
    private GameObject _pelvis_node;
    private GameObject _torso_node;
    private GameObject _neck_node;
    private GameObject _left_shoulder_node;
    private GameObject _right_shoulder_node;
    private GameObject _left_elbow_node;
    private GameObject _right_elbow_node;
    private GameObject _left_buttcheek_node;
    private GameObject _right_buttcheek_node;
    private GameObject _left_knee_node;
    private GameObject _right_knee_node;
    private GameObject _left_ankle_node;
    private GameObject _right_ankle_node;
}
